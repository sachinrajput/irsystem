/**
 * 
 */
package irsystem.processquery;

import irsystem.merge.PostingList;
import irsystem.prequeryprocess.PreQuery;
import irsystem.queryresults.QueryResults;
import irsystem.sort.ScoreComparator;
import irsystem.tokenizer.TokenDetails;
import irsystem.util.DataWriter;
import irsystem.util.Debug;
import irsystem.util.Helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import java.util.Map.Entry;

public class ProcessQuery {
	private Hashtable<String, Integer> tokenTermFq = new Hashtable<String, Integer>();
	private Hashtable<String, Object> docDict;
	private Hashtable<String, Object> queryDict = new Hashtable<String, Object>();
	private Hashtable<Integer, String> docIDTokens = new Hashtable<Integer, String>();
	private ArrayList<QueryResults> finalScores = new ArrayList<QueryResults>();
	
	private PreQuery preQueryobj;
	Helper helper = new Helper();
	
	public ProcessQuery(Hashtable<String, Object> docDictIn, PreQuery preQueryobjIn) {
		// TODO Auto-generated constructor stub
		setDocDict(docDictIn);
		setPreQueryobj(preQueryobjIn);
	}
	/**
	 * @return the docDict
	 */
	public Hashtable<String, Object> getDocDict() {
		return docDict;
	}

	/**
	 * @param docDict the docDict to set
	 */
	public void setDocDict(Hashtable<String, Object> docDict) {
		this.docDict = docDict;
	}

	/**
	 * @return the tokenTermFq
	 */
	public Hashtable<String, Integer> getTokenTermFq() {
		return tokenTermFq;
	}

	/**
	 * @param tokenTermFq the tokenTermFq to set
	 */
	public void setTokenTermFq(Hashtable<String, Integer> tokenTermFq) {
		this.tokenTermFq = tokenTermFq;
	}

	/**
	 * @return the preQueryobj
	 */
	public PreQuery getPreQueryobj() {
		return preQueryobj;
	}

	/**
	 * @param preQueryobj the preQueryobj to set
	 */
	public void setPreQueryobj(PreQuery preQueryobj) {
		this.preQueryobj = preQueryobj;
	}

	/**
	 * @return the queryDict
	 */
	public Hashtable<String, Object> getQueryDict() {
		return queryDict;
	}
	/**
	 * @param queryDict the queryDict to set
	 */
	public void setQueryDict(Hashtable<String, Object> queryDict) {
		this.queryDict = queryDict;
	}
	/**
	 * @return the docIDTokens
	 */
	public Hashtable<Integer, String> getDocIDTokens() {
		return docIDTokens;
	}
	/**
	 * @param docIDTokens the docIDTokens to set
	 */
	public void setDocIDTokens(Hashtable<Integer, String> docIDTokens) {
		this.docIDTokens = docIDTokens;
	}
	
	
	public void step1() {
		// TODO Auto-generated method stub
		
		Set<Entry<String, Integer>> tokenTermFqset = tokenTermFq
				.entrySet();
		Iterator<Entry<String, Integer>> tokenTermFqitr = tokenTermFqset
				.iterator();
		Entry<String, Integer> currentEntry;

		while (tokenTermFqitr.hasNext()) {
			currentEntry = tokenTermFqitr.next();
			String queryToken = currentEntry.getKey();
			// lets fire a search on our HT1 ->
			//System.out.println("Step 1: lets fire a search with stemmed: " + tokenforq+" on our HT1 -> qterm : " + queryToken +" tfq :"+currentEntry.getValue());
			// tokenObj.getTokenDict()
			//System.out.println(tokenObj.getTokenDict().containsKey(tokenforq));
			if (docDict.containsKey(queryToken)) {
				
				TokenDetails tokenDetObj = (TokenDetails) docDict.get(queryToken);
				queryDict.put(queryToken, tokenDetObj);

				ArrayList<PostingList> postinglist = tokenDetObj
						.getPostingList();
				Iterator<PostingList> postinglistitr = postinglist
						.iterator();
				PostingList currentpostinglist;
				
				while (postinglistitr.hasNext()) {
					currentpostinglist = postinglistitr.next();
					Integer postdocID = currentpostinglist
							.getDocID();
					if (!docIDTokens.containsKey(postdocID)) {
						//System.out.println("new docid found puting in ht");
						docIDTokens.put(postdocID, queryToken);
					} else {
						
						String prevTokenstring = docIDTokens
								.get(postdocID);
						docIDTokens.put(postdocID, prevTokenstring
								+ "," + queryToken);
						//System.out.println("docid already there .... so appended value - " + prevTokenstring
							//	+ "," + queryToken);
					}
				}
			} else {

			}
		} // collection of tokenTermFq => token+tf | queryDict => token + object->df,posting list | docIDTokens=> docid + csv of query tokens
		
	}
	
	public void step2(){
		Set<Entry<Integer,String>> docIDTokenset = docIDTokens
				.entrySet();
		Iterator<Entry<Integer,String>> docIDTokenitr = docIDTokenset
				.iterator();
		Entry<Integer,String> currendocIDTokentEntry;
		
		
		while(docIDTokenitr.hasNext()){
			currendocIDTokentEntry = docIDTokenitr.next();
			Integer currdocID = currendocIDTokentEntry.getKey();
			//iterate over queryDict 
			Set<Entry<String, Object>> queryDictset = queryDict
					.entrySet();
			Iterator<Entry<String, Object>> queryDictitr = queryDictset
					.iterator();
			Entry<String, Object> currentqueryDictlist;
			Double tempProd = 0.0;
			//System.out.println("length of queryDictitr " + queryDict.size());
			while(queryDictitr.hasNext()){
				currentqueryDictlist = queryDictitr.next();
				String qtoken = currentqueryDictlist.getKey();
				
				//first use docid 
				Hashtable<String,Double> termwt_ht = preQueryobj.getTermWt().get(currdocID);
				Double prod = 0.0;
				if(termwt_ht.containsKey(qtoken)){
					//System.out.println("found:"+qtoken);
					Double currTermWt = termwt_ht.get(qtoken);
					
					
					Double normalizedwtTerm = currTermWt / preQueryobj.getNormFact()[currdocID];
					
//					if(qtoken.equals("nike") || qtoken.equals("climb")){
//						System.out.println(currdocID+" normalizedwtTerm for "+currTermWt+"/"+preQueryobj.getNormFact()[currdocID]+"="+qtoken+": "+normalizedwtTerm);
//					}
					
					TokenDetails tokenObj = (TokenDetails)currentqueryDictlist.getValue();
					//Calculate tf-wt of query 
					Double tfwt = 0.0;
					if(tokenTermFq.containsKey(qtoken)){
						tfwt = 1+Math.log10(tokenTermFq.get(qtoken));
					} 
					Integer df = tokenObj.getDocFreq();
					
					Double idf = Math.log10(Debug.getNumberofDocs_VALUE()/df);
					
					Double querywt = tfwt * idf;
					
					/* if(qtoken.equals("nike") || qtoken.equals("climb")){
						System.out.println(currdocID + "querywt for "+qtoken+": "+querywt);
					} */
					prod = querywt*normalizedwtTerm;
				} //else System.out.println("not f"+qtoken);
				
				
				tempProd = tempProd + prod;
			}
			//now we have calculated score for this docid with all term wts of QUERY AND DOCUMENT in tempProd
			finalScores.add(helper.createQueryResultsStructure(currdocID, tempProd));
		}
	}
	
	public void cleartables(){
		// TODO Auto-generated method stub
		if(!tokenTermFq.isEmpty())
			tokenTermFq.clear();
		if(!finalScores.isEmpty())
			finalScores.clear();
		if(!queryDict.isEmpty())
			queryDict.clear();
		if(!docIDTokens.isEmpty())
			docIDTokens.clear();
	}
	
	public void step3(int index,String fullQuery) {
		// TODO Auto-generated method stub
		DataWriter queryresWriter = new DataWriter("query"+index+"result.txt");
		
		Collections.sort(finalScores,new ScoreComparator());
		
		Iterator<QueryResults> queryresitr = finalScores.iterator();
		QueryResults currentqueryreslist;
		
		int counter = 0;
		Debug.printDebugLn(0,"For query term : " + fullQuery);
		Debug.printDebugLn(1,"For query term : " + fullQuery);
		while(queryresitr.hasNext()){
			++counter;
			currentqueryreslist = queryresitr.next();
			String queryResult = currentqueryreslist.getDocID() +" "+currentqueryreslist.getScore();
			Debug.printDebugLn(0,queryResult);
			Debug.printDebugLn(1,queryResult);
			queryresWriter.writeLn(queryResult);
			if(counter>9) break;
		}
		Debug.printDebugLn(0, "");
		Debug.printDebugLn(1, "");
		queryresWriter.close();
		
	}
	
}
