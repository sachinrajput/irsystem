/**
 * 
 */
package irsystem.prequeryprocess;

import irsystem.merge.PostingList;

import irsystem.tokenizer.TokenDetails;
import java.util.ArrayList;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class PreQuery {
	private Hashtable<Integer,Hashtable<String,Double>> termWt;
	private Double[] normFact = new Double[200];

	public PreQuery(){
		setTermWt(new Hashtable<Integer, Hashtable<String,Double>>());
	}
	
	/**
	 * @return the termWt
	 */
	public Hashtable<Integer, Hashtable<String, Double>> getTermWt() {
		return termWt;
	}

	/**
	 * @param termWt the termWt to set
	 */
	public void setTermWt(Hashtable<Integer, Hashtable<String, Double>> termWt) {
		this.termWt = termWt;
	} 
	
	public void processStep1(Hashtable<String,Object> tokenDict){
		
		Set<Entry<String, Object>> tokenSet = tokenDict.entrySet();
		Iterator<Entry<String, Object>> itr = tokenSet.iterator();
		Entry<String, Object> currentObj;
		
		while(itr.hasNext()){
			currentObj = itr.next();
			String token = currentObj.getKey();
			TokenDetails tokenDetailsObj = (TokenDetails) currentObj.getValue();
			
			int docFreq = tokenDetailsObj.getDocFreq();
			ArrayList<PostingList> postinglist = tokenDetailsObj.getPostingList();

			//Collections.sort(postinglist, new DocIDPostingComparator());
			
			Iterator<PostingList> itr1 = postinglist.iterator();
			PostingList currentObj1;
			while (itr1.hasNext()) {
				currentObj1 = itr1.next();
				int docID = currentObj1.getDocID();
				int termfq = currentObj1.getTermFreq();
				double termfqwt = this.calcTFWt(termfq);
				
				//String postData = docID + " " + termfq;
				this.saveTotermWt(docID, token, termfqwt);
				
				//Debug.printDebug(Debug.getDebugValue(),postData);
			}
			
		}
	}
	
	public void processStep2(){
		Set<Entry<Integer, Hashtable<String, Double>>> tokenSet = this.getTermWt().entrySet();
		Iterator<Entry<Integer, Hashtable<String, Double>>> itr = tokenSet.iterator();
		Entry<Integer, Hashtable<String, Double>> currentObj;
		
		
		
		while(itr.hasNext()){
			currentObj = itr.next();
			Integer docid = currentObj.getKey();
			
			Set<Entry<String, Double>> tokenSet1 = currentObj.getValue().entrySet();
			Iterator<Entry<String, Double>> itr1 = tokenSet1.iterator();
			Entry<String, Double> currentObj1;
			Double temp = 0.0;
			//System.out.println(" starting process2 for docid : "+docid);
			while(itr1.hasNext()){
				currentObj1 = itr1.next();
				Double tempSq = currentObj1.getValue() * currentObj1.getValue();
				
				temp = temp + tempSq;
				//System.out.println("value of temp - " + temp + " for docid :"+docid);
				//this.getNormFact().add(e)
			}
			//System.out.println("value of temp - " + temp + " for docid :"+docid);
			temp = Math.sqrt(temp);
			//System.out.println("value of tem1p - " + temp + " for docid :"+docid);
			
			normFact[docid] = temp;
			temp = 0.0;
		}
	}
	
	public double calcTFWt(Integer termFreq){
		return (1+ Math.log10(termFreq));
	}
	
	public void saveTotermWt(int docID,String token, double termfqwt){
		
		if(!this.getTermWt().containsKey(docID)){
			//add new 
			//Create hashtable for storing entries of tokens for this document
			Hashtable<String,Double> docEntry = new Hashtable<String, Double>();
			docEntry.put(token, termfqwt);
			this.getTermWt().put(docID, docEntry);
		} else {
			//update
			Hashtable<String,Double> oldValue = this.getTermWt().get(docID); 
			oldValue.put(token, termfqwt);
		}
	}

	/**
	 * @return the normFact
	 */
	public Double[] getNormFact() {
		return normFact;
	}

	/**
	 * @param normFact the normFact to set
	 */
	public void setNormFact(Double[] normFact) {
		this.normFact = normFact;
	}
	
}
